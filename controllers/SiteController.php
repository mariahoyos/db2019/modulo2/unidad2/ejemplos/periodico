<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use app\models\Autores;
use app\models\Fotografos;
use app\models\Etiquetas;
use app\models\Categorizan;
use app\models\NoticiasForm;
use app\models\FormRegister;
use app\models\Usuarios;
use yii\helpers\Html;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        //si quiero conocer el nombre de usuario
        //var_dump(Yii::$app->user->identity);
        //exit;
        //
        //
        // en caso de no estar logueado nos colocamos en la pagina de inicio
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        

        $model = new LoginForm();
        // en caso de intentar realizar un logueo
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // si es correcto volvemos a la pagina anterior
            return $this->goBack();
        }
        
        // en caso de que el logueo no sea correcto no entramos
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionLogout()
    {
        // nos salimos de la sesion
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
    
    public function actionUrl(){
        echo Html::a("texto","@web/index.php/site/confirm?id=1&authKey=2");
        echo Html::a('Confirmar registro', "@web/index.php/site/confirm?id=1", ['class' => 'profile-link']);
        echo Html::a('Confirmar registro',"http://localhost" . Url::to(["site/confirm"]));
    }


    public function actionRegister()
    {
         //Creamos la instancia con el model de validación
         $model = new FormRegister;

         //Mostrará un mensaje en la vista cuando el usuario se haya registrado
         $msg = null;

         //Validación mediante ajax
       //  if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
       //        {
       //            Yii::$app->response->format = Response::FORMAT_JSON;
       //            return ActiveForm::validate($model);
       //        }

         //Validación cuando el formulario es enviado vía post
         //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
         //También previene por si el usuario tiene desactivado javascript y la
         //validación mediante ajax no puede ser llevada a cabo
         if ($model->load(Yii::$app->request->post())) //se cogen todos los datos que vienen del formulario
         {

          if($model->validate())//comprueba las rules del modelo (FormRegister)
          {
           //Preparamos la consulta para guardar el usuario
           $table = new Usuarios;
           $table->usuario = $model->usuario;
           $table->email = $model->email;
           //Encriptamos el password
           $table->password = crypt($model->password, Yii::$app->params["salt"]);
           //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
           //clave será utilizada para activar el usuario
           $table->authKey = $this->randKey("abcdef0123456789", 200);
           //Creamos un token de acceso único para el usuario
           $table->accessToken = $this->randKey("abcdef0123456789", 200);

           //Si el registro es guardado correctamente
           if ($table->insert())//save si existe lo va a actualizar
           {
            //Nueva consulta para obtener el id del usuario
            //Para confirmar al usuario se requiere su id y su authKey
            $user = $table->find()->where(["email" => $model->email])->one();
            $id = urlencode($user->id);//formatear ese dato para que sea admitido por la URL (como los espacios en blanco)
            $authKey = urlencode($user->authKey);

            $subject = "Confirmar registro";
           
            $body = "<h1>Haga click en el siguiente enlace para finalizar tu registro</h1>";
            //$body .= "<a href='http://127.0.0.1/.../index.php?r=site/confirm&id=".$id."&authKey=".$authKey."'>Confirmar</a>";
            $body .= Html::a('Confirmar registro', "http://localhost/" . Yii::getAlias("@web") . "/index.php/site/confirm?id=$id&authKey=$authKey", ['class' => 'profile-link']); 
            //cambiar por HTML::a... hasta r= 
            //los parametros sean $id y $authKey

           // $body .= Html::a("Confirmar",[site/confirmar,"id"=>$id,"authKey"=>$authKey]);

            //Enviamos el correo
            Yii::$app->mailer->compose()
            ->setTo($user->email)
            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
            ->setSubject($subject)
            ->setHtmlBody($body)
            ->send();

            $model->usuario = null;
            $model->email = null;
            $model->password = null;
            $model->password_repeat = null;

            $msg = "Enhorabuena, ahora sólo falta que confirmes tu registro en tu cuenta de correo";
           }
           else
           {
            $msg = "Ha ocurrido un error al llevar a cabo tu registro";
           }

          }
          else
          {
           $model->getErrors();
          }
         }
         return $this->render("register", ["model" => $model, "msg" => $msg]);
        }
        
    public function actionConfirm()
    {
       $table = new Usuarios;
       if (Yii::$app->request->get())
       {

           //Obtenemos el valor de los parámetros get
           $id = $_GET["id"];
           $authKey = $_GET["authKey"];

           if ((int) $id)
           {
               //Realizamos la consulta para obtener el registro
               $model = $table
               ->find()
               ->where("id=:id", [":id" => $id])
               ->andWhere("authKey=:authKey", [":authKey" => $authKey]);

               //Si el registro existe
               if ($model->count() == 1)
               {
                   $activar = Usuarios::findOne($id);
                   $activar->activate = 1;
                   if ($activar->update())
                   {
                       echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                       echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                   }
                   else
                   {
                       echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                       echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                   }
                }
               else //Si no existe redireccionamos a login
               {
                   return $this->redirect(["site/login"]);
               }
           }
           else //Si id no es un número entero redireccionamos a login
           {
               return $this->redirect(["site/login"]);
           }
       }
    }
    
    /**
     * Displays frontend page for noticias.
     *
     * 
     */
    public function actionIndexnoticias()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('vista1', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "titulo"=>"Noticias de nuestro periódico",
          ]);
    }
    
     /**
     * Displays frontend page for autores.
     *
     * 
     */
    public function actionIndexautores()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Autores::find(),
        ]);

        return $this->render('vista2', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "titulo"=>"Autores de nuestro periódico",
          ]);
    }
    
     /**
     * Displays frontend page for fotografos.
     *
     * 
     */
    public function actionIndexfotografos()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fotografos::find(),
        ]);

        return $this->render('vista3', [
            "dataProvider" => $dataProvider,
            "campos"=>['texto'],
            "titulo"=>"Fotógrafos de nuestro periódico",
          ]);
    }
    
     /**
     * Displays frontend page for fotografos.
     *
     * 
     */
    public function actionIndexetiquetas()
    {
       
        
        $numnoticias = Etiquetas::find()
                ->select("etiquetas.id,etiqueta,count(*) numeroNoticias,count(distinct idautor) numeroAutores")
                ->innerJoinWith("noticias")
                ->groupBy('etiquetas.id,etiqueta');
        
        
         $dataProvider = new ActiveDataProvider([
            'query' => $numnoticias,
        ]);
        
        
        //var_dump($numnoticias);
                
        

        return $this->render('vista4', [
            "dataProvider" => $dataProvider,
            "titulo"=>"Etiquetas de nuestro periódico",
            
          ]);
    }
    
    

    

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    public function actionEnviarnoticia()
    {
        $model = new NoticiasForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('Envionoticia');
            //evitar envio masivo del correo con F5
            return $this->refresh();
        }
        return $this->render('envionoticia', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
