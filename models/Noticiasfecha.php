<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticiasfecha".
 *
 * @property int $id
 * @property int $idnoticia
 * @property string $fecha_publicacion
 *
 * @property Noticias $noticia
 */
class Noticiasfecha extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticiasfecha';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idnoticia'], 'integer'],
            [['fecha_publicacion'], 'safe'],
            [['idnoticia', 'fecha_publicacion'], 'unique', 'targetAttribute' => ['idnoticia', 'fecha_publicacion']],
            [['idnoticia'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['idnoticia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idnoticia' => 'Idnoticia',
            'fecha_publicacion' => 'Fecha Publicacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticia()
    {
        return $this->hasOne(Noticias::className(), ['id' => 'idnoticia']);
    }
    
     public function afterFind() {
        parent::afterFind();
        $this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:d-m-Y');
        
    }
    // modificar la fecha que antes de grabar la devuelva en formato YYYY/m/d
//    public function beforeSave($insert) {
//        if(parent::beforeSave($insert)){
//           $this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:Y-m-d');
//           return true;
//        }
//    }
    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          //$this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:Y-m-d');
          $this->fecha_publicacion= \DateTime::createFromFormat("d/m/Y", $this->fecha_publicacion)->format("Y/m/d");
          return true;
    }
    
    

}
