<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class NoticiasForm extends Model
{
    public $name;
    public $autor;
    public $alias;
    public $titulo;
    public $texto;
    public $etiquetas;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['autor', 'texto', 'titulo'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            // las etiquetas tienen que tener una longitud máximo de 50
            [['etiquetas'],'string','max'=>50],
            // el alias tiene que ser cadena
            [['alias'],'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Código de verificación',
            'alias' => 'Alias del autor',
            'email' => 'Dirección de email',
            'autor' => 'Nombre del autor',
            'texto' => 'Cuerpo de la noticia',
            'titulo' => 'Título de la noticia',
            'etiquetas' => 'Etiquetas de la noticia',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->autor])
                ->setSubject($this->alias)
                ->setTextBody('Título de la noticia: '.$this->titulo.' Texto de la noticia: '.$this->texto.' Etiquetas: '.$this->etiquetas. 'Alias: '.$this->alias)
                ->send();

            return true;
        }
        return false;
    }
    
    
}
