<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    public static function tableName()
    {
        return 'usuarios';
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        //no encuentra el usuario y no puede validar tampoco la password
//    return static::findOne(['usuario' => $username]);
       
         $users = Usuarios::find()           
                ->Where("usuario=:usuario", [":usuario" => $username])
                ->all();
       
        foreach ($users as $user) {
            if (strcasecmp($user->usuario, $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }
    

     /**
     * @inheritdoc
     */
    public function getId()
    {
        //return $this->getPrimaryKey();//no retorna el id
          return $this->id;
    }
    
    /*public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }*/
    
    
//    public function validatePassword($password)
//    {
//        //return $this->password === $password;
//        if (crypt($password, $this->password) == $this->password)
//        {
//        return true;
//        }
//    }
    
    public function validatePassword($password)
    {
       //Sin encriptación
   
        if ($password == $this->password)
        {
        return $password === $password;
        }
    }
    
     /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }
    
}
