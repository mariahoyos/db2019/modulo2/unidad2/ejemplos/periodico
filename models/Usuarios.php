<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 */
class Usuarios extends \yii\db\ActiveRecord
{
    
    public static function getDb()
    {
        return Yii::$app->db;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['usuario', 'password'], 'string', 'max' => 50],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Nombre de usuario',
            'password' => 'Contraseña',
        ];
    }
}
