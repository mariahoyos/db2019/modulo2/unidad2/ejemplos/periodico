<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categorizan */

$this->title = 'Create Categorizan';
$this->params['breadcrumbs'][] = ['label' => 'Categorizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categorizan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
