<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ilustranfotos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ilustranfotos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idilustran')->textInput() ?>

    <?= $form->field($model, 'fotos')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
