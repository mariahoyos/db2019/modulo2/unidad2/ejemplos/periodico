<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\grid\GridView;

$this->title = "Etiquetas";
//var_dump($numnoticias);
//exit;

?>

<div class="well well-sm"><h2 style="text-align: center;max-height: 50px;"><?= $titulo ?></h2></div>
<p>
        <?= Html::a('Crear Etiqueta', ['create'], ['class' => 'btn btn-warning']) ?>
    </p>

<div class="etiquetas-index">

   

    
     
    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'etiqueta',
            'numeroNoticias',
            'numeroAutores',
            ['class' => 'yii\grid\ActionColumn',
            'header' => 'Noticias relacionadas',
            'template'=>'{noticias}',    
            'buttons'=>[
                'noticias'=>function($url,$model){
                    return Html::a('Ver Noticias', ['noticias/noticiasetiqueta','etiqueta'=>$model->id], ['class' => 'btn btn-primary btn-md']);
                    
                },
            ],     
            ],
                       
            
            
        ],
                        
        'rowOptions' =>['style'=>'text-align:center;'],
        'layout'=>"{summary}\n{pager}\n{items}",
        
    ]); ?>


</div>

