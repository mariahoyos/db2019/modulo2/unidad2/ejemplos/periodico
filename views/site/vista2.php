<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
?>

<!--<div class="jumbotron">
  <h2><?= $titulo ?></h2>
  

  
</div>-->

<div class="well well-sm"><h2 style="text-align: center;max-height: 50px;"><?= $titulo ?></h2></div>

<p>
        <?= Html::a('Crear Autor', ['create'], ['class' => 'btn btn-warning']) ?>
</p>

<div class="row">
<?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_vista2',
            'layout'=>"{summary}\n{pager}\n{items}",
        ]);
?>
</div>