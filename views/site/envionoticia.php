<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Enviar Noticia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('Envionoticia')){ ?>

        <div class="alert alert-success">
            Gracias por enviarnos vuestra noticia
        </div>

    <?php }else{ ?>

        <p>
            Para ponerte en contacto con nosotros envianos un formulario
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'autor')->textInput(['autofocus' => true]) ?>
                
                    <?= $form->field($model, 'alias') ?>
                
                    <?= $form->field($model, 'titulo') ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'texto')->textarea(['rows' => 6]) ?>
                
                    <?= $form->field($model, 'etiquetas') ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        'imageOptions' => [
                            'id' => 'my-captcha-image'
                        ],

                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php } ?>
</div>
