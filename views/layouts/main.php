<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    if(Yii::$app->user->isGuest){
        echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Login', 'url' => ['/site/login']],
            ['label' => 'Noticias', 'url' => ['/site/indexnoticias']],
            ['label' => 'Autores', 'url' => ['/site/indexautores']],
            ['label' => 'Fotógrafos', 'url' => ['/site/indexfotografos']],
            ['label' => 'Etiquetas', 'url' => ['/site/indexetiquetas']],
            ['label' => 'Enviar Noticia', 'url' => ['/site/enviarnoticia']],
        ],
    ]);
    }else{
        echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Autores', 'url' => ['/autores/index']],//'visible'=>Yii::app()->user->identity->isAdmin],//,'visible'=>Yii::app()->user->identity->isAdmin?
            ['label' => 'Noticias', 'url' => ['/noticias/index']],
            ['label' => 'Fotógrafos', 'url' => ['/fotografos/index']],
            //['label' => 'Ilustran Fotos', 'url' => ['/ilustranfotos/index']],
            ['label' => 'Etiquetas', 'url' => ['/etiquetas/index']],
            ['label' => 'Noticias', 'url' => ['/site/indexnoticias']],
            ['label' => 'Autores', 'url' => ['/site/indexautores']],
            ['label' => 'Fotógrafos', 'url' => ['/site/indexfotografos']],
            ['label' => 'Etiquetas', 'url' => ['/site/indexetiquetas']],
            ['label' => 'Contacto', 'url' => ['/site/contact']],
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->email . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>',
        ],
    ]);
    }
    
    
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
