<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ilustran */

$this->title = 'Create Ilustran';
$this->params['breadcrumbs'][] = ['label' => 'Ilustrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ilustran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
