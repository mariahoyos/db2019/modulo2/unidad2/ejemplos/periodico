<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */

$this->title = "Información del autor: ".$model->nombre;

$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="jumbotron">
  <h1 class="display-4"><?= $model->nombre." - ".$model->id ?></h1>
  <p class="lead"><?= "Alias: ".$model->alias ?></p>
  <hr class="my-4">
  <p><?= "Correo electrónico: ".$model->email ?></p>
  <p class="lead">
      <?= Html::a('Ver Noticias', ['noticias/noticiasautor','autor'=>$model->id], ['class' => 'btn btn-primary btn-lg']) ?>
    
  </p>
</div>